import os
import pdb
import cv2
import numpy as np
import random
from numpy.core.numeric import full
from numpy.lib.function_base import flip
from scipy import ndarray
import skimage as sk
from skimage import transform
from skimage import util
import matplotlib.pylab as plt
import imgaug as ia
import imgaug.augmenters as iaa
import argparse


def make_output_dirs(args):
    full_output_path = os.path.join(args.output_path, "synthetic_training_data")

    if os.path.exists(args.output_path):
        overwrite_existing = input(
            "Output path exists, overwrite existing data? [y/n]: "
        )
        if overwrite_existing.lower().find("n") != -1:
            new_output_path = input("Enter new fully quantified output path: ")
            full_output_path = os.path.join(new_output_path, "synthetic_training_data")

    os.makedirs(full_output_path, exist_ok=True)
    os.makedirs(os.path.join(full_output_path, "images"), exist_ok=True)
    return full_output_path


def just_fuck_my_shit_up(img, add_noise, max_rotation):

    rotation = np.random.randint(max_rotation)
    if np.random.rand() > 0.5:
        rotation = -1 * rotation
    
    random_rescale = np.random.rand()
    if(random_rescale < 0.25):
        random_rescale = 0.25
    new_height = int(random_rescale * img.shape[0])
    new_width = int(random_rescale * img.shape[1])
    
    alpha = img[:, :, 3]
    bgr = img[:, :, 0:3]

    bgr = cv2.resize(bgr, (new_width, new_height), interpolation=cv2.INTER_AREA)
    hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    random_shift = np.random.randint(30)

    hnew = np.mod(h + random_shift, 180).astype(np.uint8)
    hsv_new = cv2.merge([hnew, s, v])
    bgr_new = cv2.cvtColor(hsv_new, cv2.COLOR_HSV2BGR)
    bgra = cv2.cvtColor(bgr_new, cv2.COLOR_BGR2BGRA)
    bgra[:, :, 3] = cv2.resize(
        alpha, (new_width, new_height), interpolation=cv2.INTER_AREA
    )
    rotated = sk.transform.rotate(bgra, rotation)

    if np.random.rand() > 1.0:
        flipadelphia = rotated[:, ::-1]
    else:
        flipadelphia = rotated
    if add_noise:
        final = sk.util.random_noise(flipadelphia)
    else:
        final = flipadelphia
    return final * 255


def draw_bounding_box(img, top_left, bottom_right):

    box_width = bottom_right[1] - top_left[1]
    box_height = bottom_right[0] - top_left[0]
    center_x = int(top_left[1] + box_width / 2)
    center_y = int(top_left[0] + box_height / 2)
    rectangle_color = (0, 0, 255)
    circle_color = (0, 255, 0)
    radius = 10
    rectangle_thickness = 3
    circle_thickness = -1  # Fill with color
    cv2.rectangle(img, top_left, bottom_right, rectangle_color, rectangle_thickness)
    cv2.circle(img, (center_y, center_x), radius, circle_color, circle_thickness)

    return img


def paste_foreground_in_background(foreground, background):

    foreground = just_fuck_my_shit_up(foreground, False, 90)
    height, width, depth = background.shape
    dX = np.random.randint(width - 10 - foreground.shape[1])
    dY = np.random.randint(height - 10 - foreground.shape[0])
    row, col = np.nonzero(foreground[:, :, 3])

    new = background.copy()
    new = cv2.cvtColor(new, cv2.COLOR_BGR2BGRA)

    if max(row) + dY >= height:
        print("Reshaping dY")
        while max(row) + dY > height - 2:
            dY -= 10
    if max(col) + dX >= width:
        print("Reshaping dX")
        while max(col) + dX > width - 2:
            dX -= 10
    new[row + dY, col + dX, :] = foreground[row, col]

    top_left = (dX, dY)
    bottom_right = (dX + foreground.shape[1], dY + foreground.shape[0])
    # new = draw_bounding_box(new, top_left, bottom_right)
    return foreground.shape[0], foreground.shape[1], dY, dX, new


def resize_image(fname, max_dim):
    raw_image = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
    if [x for x in raw_image.shape if x > max_dim]:
        fx = max_dim / raw_image.shape[1]
        fy = max_dim / raw_image.shape[0]
        width = int(raw_image.shape[1] * fx)
        height = int(raw_image.shape[0] * fy)
        img = cv2.resize(raw_image, (width, height), interpolation=cv2.INTER_AREA)
    else:
        img = raw_image

    return img


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "backgrounds", help="Directory containing background images", type=str,
    )
    parser.add_argument(
        "objects",
        help="Directory containing objects to be pasted on to backgrounds",
        type=str,
    )
    parser.add_argument(
        "--num-objects",
        help="Number of ojects to paste into each background. Default=10",
        type=int,
        default=10,
    )
    parser.add_argument(
        "--output-path",
        help="Path where output images and bounding box label file will be saved. Default is current working directory.",
        type=str,
        default="./",
    )
    args = parser.parse_args()

    full_output_path = make_output_dirs(args)

    logfile = open(os.path.join(full_output_path, "train_df.csv"), "w")
    logfile.write("image_id,num_dew_bottles,x,y,w,h\n")
    background_list = os.listdir(args.backgrounds)
    background_list.sort()
    object_list = os.listdir(args.objects)

    for n, b in enumerate(background_list):
        current_background = os.path.join(args.backgrounds, b)
        print(b)
        tmp = cv2.imread(current_background, cv2.IMREAD_COLOR)
        output_image_name = "{:04d}_synth.jpg".format(n)
        for i in range(args.num_objects):
            obj_index = np.random.randint(len(object_list))
            current_object = os.path.join(args.objects, object_list[obj_index])
            foreground = resize_image(current_object, int(max(tmp.shape) * 0.25))
            if i == 0:
                current_background = cv2.imread(
                    current_background, cv2.IMREAD_COLOR
                )
                try:
                    height, width, dY, dX, new = paste_foreground_in_background(
                        foreground, current_background
                    )
                except Exception as e:
                    print("################################")
                    print("Got exception {} on {}".format(e, b))
                    continue
            else:
                try:
                    height, width, dY, dX, new = paste_foreground_in_background(
                        foreground, new
                    )
                except Exception as e:
                    print("################################")
                    print("Got exception {} on {}".format(e, b))
                    continue
            logfile.write(
                "{:04d}_synth,{},{},{},{},{}\n".format(
                    n, args.num_objects, dX, dY, width, height
                )
            )
        cv2.imwrite(os.path.join(full_output_path, "images", output_image_name), new)


if __name__ == "__main__":
    main()
