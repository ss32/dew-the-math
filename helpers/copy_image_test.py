import os
import pdb
import cv2
import numpy as np
import random
from numpy.lib.function_base import flip
from scipy import ndarray
import skimage as sk
from skimage import transform
from skimage import util
import matplotlib.pylab as plt
import imgaug as ia
import imgaug.augmenters as iaa
import argparse


def draw_bounding_box(img, top_left, bottom_right):

    box_width = bottom_right[1] - top_left[1]
    box_height = bottom_right[0] - top_left[0]
    center_x = int(top_left[1] + box_width / 2)
    center_y = int(top_left[0] + box_height / 2)
    rectangle_color = (0, 0, 255)
    circle_color = (0, 255, 0)
    radius = 10
    rectangle_thickness = 3
    circle_thickness = -1  # Fill with color
    img = cv2.rectangle(img, top_left, bottom_right, rectangle_color, rectangle_thickness)
    img = cv2.circle(img, top_left, radius, rectangle_color, circle_thickness)
    img = cv2.circle(img, bottom_right, radius, (255, 0, 0), circle_thickness)
    img = cv2.circle(img, (center_y, center_x), radius, circle_color, circle_thickness)

    return img


def resize_image(fname, max_dim):
    raw_image = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
    if [x for x in raw_image.shape if x > max_dim]:
        fx = max_dim / raw_image.shape[1]
        fy = max_dim / raw_image.shape[0]
        width = int(raw_image.shape[1] * fx)
        height = int(raw_image.shape[0] * fy)
        img = cv2.resize(raw_image, (width, height), interpolation=cv2.INTER_AREA)
    else:
        img = raw_image
    return img

def just_fuck_my_shit_up(img, add_noise):

    alpha = img[:, :, 3]
    bgr = img[:, :, 0:3]
    hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    random_shift = np.random.randint(360)

    hnew = np.mod(h + random_shift, 180).astype(np.uint8)
    hsv_new = cv2.merge([hnew, s, v])
    bgr_new = cv2.cvtColor(hsv_new, cv2.COLOR_HSV2BGR)
    bgra = cv2.cvtColor(bgr_new, cv2.COLOR_BGR2BGRA)
    bgra[:, :, 3] = alpha
    rotated = sk.transform.rotate(bgra, random_shift)

    if np.random.rand() > 0.5:
        flipadelphia = rotated[:, ::-1]
    else:
        flipadelphia = rotated
    if add_noise:
        final = sk.util.random_noise(flipadelphia)
    else:
        final = flipadelphia
    return final * 255

def paste_foreground_in_background(foreground, background):
    
    foreground = just_fuck_my_shit_up(foreground, False)
    height, width, depth = background.shape
    dX = np.random.randint(width)
    dY = np.random.randint(height)
    row, col = np.nonzero(foreground[:, :, 3])
    
    new = background.copy()
    # new = np.dstack((new, np.ones_like(new[:, :, 0]).astype('uint8')))
    new = cv2.cvtColor(new, cv2.COLOR_BGR2BGRA)

    if max(row) + dY >= height:
        while max(row) + dY > height - 2:
            dY -= 10
    if max(col) + dX >= width:
        while max(col) + dX > width - 2:
            dX -= 10
    new[row + dY, col + dX,:] = foreground[row, col]

    top_left = (dX, dY)
    bottom_right = (dX + foreground.shape[1], dY + foreground.shape[0])
    print(dX, dY, top_left, bottom_right)
    new = draw_bounding_box(new, top_left, bottom_right)
    return dY, dX, new


background = cv2.imread("background.jpg", cv2.IMREAD_UNCHANGED)
foreground = resize_image("test.png", 300)
dY, dX, new = paste_foreground_in_background(foreground, background)
for i in range(10):
    dY, dX, new = paste_foreground_in_background(foreground, new)
cv2.imwrite('new.jpg',new)
# pasted = paste_foreground_in_background(foreground, background, 50, 100)
# cv2.imwrite('paste_test.png', pasted)
